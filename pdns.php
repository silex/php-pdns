<?php
class PDNS {
    # Initialise the class
    function __construct($host, $port, $api_key, $v = 'v1') {
        $this->host    = $host;
        $this->port    = $port;
        $this->api_key = $api_key;
        $this->headers = [ 'X-API-Key: ' . $api_key ];
        $this->base    = $v == 'v0' ? null : "/api/$v";
    }
    
    # Get or set info about a server
    function servers($server_id = null, $data = null) {
        $uri = '/servers';
        
        if (!is_null($server_id))
            $uri .= "/$server_id";
        
        if (is_null($data))
            return $this->get($uri);
        
        # TODO: /server: PUT, POST, DELETE
        # TODO: /servers/:server_id: ?
    }
    
    # Get or set server config
    function config($server_id, $config_setting_name = null, $data = null) {
        if (is_null($config_setting_name) && is_null($data))
          return $this->get("/servers/$server_id/config");
        if ($data == null)
          return $this->get("/servers/$server_id/$config_setting_name");

        # TODO: /config: POST
        # TODO: /config/:config_setting_name: PUT
    }
    
    # Manipulate zone data
    function zones($server_id, $zone_id = null, $data = null) {
        if (is_null($zone_id))
          return $this->server_zones($server_id, $data);
        
        return $this->zone_zones($server_id, $zone_id, $data);
    }

    # Get zones or create one
    function server_zones($server_id, $data = null) {
        if (is_null($data))
            return get("/servers/$server_id/zones");
        return post("/servers/$server_id/zones", $rrsets);
    }

    # Manipulate specific zone data
    function zone_zones($server_id, $zone_id, $rrsets = null) {
      switch ($rrsets) {
          case null:
              return $this->get("/servers/$server_id/zones/$zone_id");
          case 'delete':
              return $this->delete("/servers/$server_id/zones/$zone_id");
          default:
              return $this->patch("/servers/$server_id/zones/$zone_id", $rrsets);
      }
      
      # TODO: /servers/:server_id/zones/:zone_id: PUT (undocumented)
    }

    ## Zone specific methods

    # Notify slaves for a zone
    function zone_notify($server_id, $zone_id) {
        return $this->put("/servers/$server_id/zones/$zone_id/notify");
    }

    # Get the AXFR for a zone
    function zone_axfr_retrieve($server_id, $zone_id) {
        return $this->put("/servers/$server_id/zones/$zone_id/axfr-retrieve");
    }

    # Export a zone
    function zone_export($server_id, $zone_id) {
        return $this->get("/servers/$server_id/zones/$zone_id/export");
    }

    # Check a zone
    function zone_check($server_id, $zone_id) {
        return $this->get("/servers/$server_id/zones/$zone_id/check");
    }

    # Manipulate metadata for a zone
    function metadata($server_id, $zone_id, $metadata_kind = null) {
      # TODO: /servers/:server_id/zones/:zone_name/metadata: GET, POST
      # TODO: /servers/:server_id/zones/:zone_name/metadata/:metadata_kind: GET, PUT, DELETE
    }

    # Change cryptokeys for a zone
    function cryptokeys($server_id, $zone_id, $cryptokey_id = null) {
      # TODO: /servers/:server_id/zones/:zone_name/cryptokeys: GET, POST
      # TODO: /servers/:server_id/zones/:zone_name/cryptokeys/:cryptokey_id: GET, PUT, DELETE
    }

    ## Server specific methods

    function server_cache($server_id, $domain) {
      # TODO: /servers/:server_id/cache/flush?domain=:domain: PUT
    }

    function server_search_log($server_id, $search_term) {
      # TODO: /servers/:server_id/search-log?q=:search_term: GET
    }

    function server_statistics($server_id) {
      # TODO: /servers/:server_id/statistics: GET
    }

    function server_trace($server_id) {
      # TODO: /servers/:server_id/trace: GET, PUT
    }

    function server_failures($server_id) {
      # TODO: /servers/:server_id/failures: GET, PUT
    }

    function server_overrides($server_id, $override_id = null) {
      # TODO: /servers/:server_id/overrides: GET, POST
      # TODO: /servers/:server_id/overrides/:override_id: GET, PUT, DELETE
    }

    # Create a Server object
    function new_server($server_id) {
      return new PDNSServer($this, $server_id);
    }

    # Create a Zone object
    function new_zone($server_id, $zone_id) {
      return new PDNSZone($this, $server_id, $zone_id);
    }

    # Do an HTTP request
    private function http($method, $uri, $body = null) {
        $curl = curl_init();
        
        if (!is_null($this->base))
            $uri = $this->base . $uri;
        
        $uri = $this->host . ':' . $this->port . $uri;
        
        switch ($method) {
            case 'GET':
                break;
            case 'PATCH':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                break;
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, 1);
                break;
            case 'PUT':
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
            exit('Unknown method: ' . $method);
        }
        
        curl_setopt($curl, CURLOPT_URL, $uri);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        if (!is_null($body))
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
        
        $response = curl_exec($curl);
        
        if (!$response)
            die(curl_error($curl));
        
        curl_close($curl);
        
        return json_decode($response, true);
    }

    # Do a DELETE request
    private function delete($uri, $body = null) {
        return $this->http('PUT', $uri, $body);
    }

    # Do a GET request
    private function get($uri) {
        return $this->http('GET', $uri);
    }

    # Do a PATCH request
    private function patch($uri, $body = null) {
        return $this->http('PATCH', $uri, $body);
    }

    # Do a POST request
    private function post($uri, $body = null) {
        return $this->http('POST', $uri, $body);
    }

    # Do a PUT request
    private function put($uri, $body = null) {
        return $this->http('PUT', $uri, $body);
    }
}

# Class for server manipulation
class PDNSServer {
    function __construct($pdns, $server_id) {
        $this->pdns      = $pdns;
        $this->server_id = $server_id;
    }

    function get($data = null) {
        return $this->pdns->servers;
    }

    function config($config_setting_name = null, $data = null) {
        return $this->pdns->config($this->server_id, $config_setting_name, $data);
    }

    function zones($data = null) {
        return $this->pdns->server_zones($this->server_id, $data);
    }

    function cache($domain) {
        return $this->pdns->server_cache($this->server_id, $domain);
    }

    function search_log($search_term) {
        return $this->pdns->server_statistics($this->server_id, $search_term);
    }

    function statistics() {
        return $this->pdns->server_statistics($this->server_id);
    }

    function trace() {
        return $this->pdns->server_trace($this->server_id);
    }

    function failures() {
        return $this->pdns->server_failures($this->server_id);
    }

    function overrides($override_id = null) {
        return $this->pdns->server_overrides($this->server_id, $override_id);
    }
}

# Class for zone manipulation
class PDNSZone {
    function __construct($pdns, $server_id, $zone_id) {
        $this->pdns      = $pdns;
        $this->server_id = $server_id;
        $this->zone_id   = $zone_id;
    }

    function delete() {
        return $this->pdns->zone_zones($this->server_id, $this->zone_id, 'delete');
    }

    function get() {
        return $this->pdns->zone_zones($this->server_id, $this->zone_id);
    }

    function put($rrset) {
        # TODO: Implement PUT (undocumented)
    }

    function modify($rrset) {
        return $this->pdns->zone_zones($this->server_id, $this->zone_id, $rrset);
    }

    function notify() {
        return $this->pdns->zone_notify($this->server_id, $this->zone_id);
    }

    function axfr_retrieve() {
        return $this->pdns->zone_axfr_retrieve($this->server_id, $this->zone_id);
    }

    function export() {
        return $this->pdns->zone_axfr_retrieve($this->server_id, $this->zone_id);
    }

    function check() {
        return $this->pdns->zone_check($this->server_id, $this->zone_id);
    }

    function metadata($metadata_kind = null) {
        return $this->pdns->metadata($this->server_id, $this->zone_id, $metadata_kind);
    }

    function cryptokeys($cryptokey_id = null) {
        return $this->pdns->cryptokeys($this->server_id, $this->zone_id, $cryptokey_id);
    }
}
